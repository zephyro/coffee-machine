$('.nav_toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.page').toggleClass('open_nav');
});


$('.header__nav ul li a').click(function(){
    $('.page').removeClass('open_nav');
    var str=$(this).attr('href');
    $.scrollTo(str, 500, {offset:-80 });
    return false;
});

$(function($){
    var header = $('.header');
    $h = 0;

    $(window).scroll(function(){
        // Если прокрутили скролл ниже макушки блока, включаем фиксацию

        if ( $(window).scrollTop() > $h) {
            header.addClass('fix_top');
        }else{
            //Иначе возвращаем всё назад. Тут вы вносите свои данные
            header.removeClass('fix_top');
        }
    });
});



$(".btn_modal").fancybox({
    'padding'    : 0,
    touch: false
});


var swiper = new Swiper('.coffee__box', {
    slidesPerView: 4,
    spaceBetween: 30,
    // init: false,
    scrollbar: {
        el: '.swiper-scrollbar',
        hide: false,
    },
    breakpoints: {
        1200: {
            slidesPerView: 4,
            spaceBetween: 20,
        },
        970: {
            slidesPerView: 3
        },
        768: {
            slidesPerView: 2
        },
        480: {
            slidesPerView: 1
        }
    }
});

$('.btn_send').click(function() {

    $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
    var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
    if(answer != false)
    {
        var $form = $(this).closest('form'),
            name = $('input[name="name"]', $form).val(),
            phone = $('input[name="phone"]', $form).val(),
            email = $('input[name="email"]', $form).val();

        $.ajax({
            type: "POST",
            url: "form-handler.php",
            data: {email:email, phone:phone, name:name}
        }).done(function(msg) {
            $('form').find('input[type=text], textarea').val('');
            console.log('удачно');
            $.fancybox.close();
            $('.btn_thanks').trigger('click');
            setTimeout('$.fancybox.close();',5000);
        });
    }
});
