<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Кофемашина в аренду бесплатно от эксперта по завариванию -  BREWEX</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="BREWEX – это бесплатная аренда кофемашин для офисов, магазинов, ресторанов, кафе или гостиниц">
	    <meta name="keywords" content="Кофемашина в аренду, аренда кофемашины бесплатно при покупке кофе, бесплатная аренда кофемашины, кофемашина в офис">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="js/vendor/swiper/css/swiper.min.css">
    
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>
    
        <div class="page">
            
            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a class="header__logo" href="/">
                            <img src="img/logo.svg" class="img-fluid" alt="">
                        </a>
                        <a class="header__phone" href="tel:+74997057378" title="+7 499 705 73 78"><span>+7 499 705 73 78</span></a>
                        <nav class="header__nav">
                            <ul>
                                <li><a href="#b02">Как это работает</a></li>
                                <li><a href="#b03">Почему мы</a></li>
                                <li><a href="#b04">Кофемашины</a></li>
                                <li><a href="#b05">Кофе в зернах</a></li>
                                <li><a href="#b06">Контакты</a></li>
                            </ul>
                        </nav>
                        <a href="#" class="header__toggle nav_toggle">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                </div>
            </header>
            <div class="nav__overlay nav_toggle"></div>
            
            <section class="first" id="b01">
                <div class="first__bg">
                    <div class="first__bg_video">
                        <video class="video" id="video" width="100%" height="auto"  autoplay="autoplay" loop="loop" preload="auto" muted>
                            <source src="video/video.mp4">
                            <source src="video/video.webm" type="video/webm">
                        </video>
                    </div>
                </div>
                <div class="first__content">
                    <div class="container">
                        <div class="first__block">
                            <h1>
                                <span>КОФЕМАШИНА В АРЕНДУ</span>
                                <strong>БЕСПЛАТНО</strong>
                            </h1>
                            <div class="first__text">Для офиса, кафе, ресторана или гостиницы</div>
                            <a href="#order" class="btn btn_md btn_modal" data-fancybox>Заказать</a>
                        </div>
                    </div>
                </div>
            </section>
            
            <section class="how">
                <div class="container">
                    <div class="how__box" id="b02">
                        <h2>Как это работает?</h2>
                        <div class="how__subtitle">Вы покупаете у нас кофе и пользуетесь кофемашиной бесплатно.</div>
                        <ul class="how__list">
                            <li>
                                <div class="how__text">Наш эксперт подбирает для вас оптимальные условия сотрудничества. Мы заключаем договор.</div>
                            </li>
                            <li>
                                <div class="how__text">Каждый месяц вы покупаете у нас кофе и сопутствующие товары в количестве, определенном договором.</div>
                            </li>
                            <li>
                                <div class="how__text">Вы получаете от нас экспертный сервис и  кофемашину, которая гарантирует стабильное качество премиального кофе.</div>
                            </li>
                        </ul>
           
                    </div>
                </div>
            </section>
            
            <section class="whu" id="b03">
                <div class="container">
                    <h2>Почему мы</h2>
                    <div class="whu__subtitle">Мы эксперты в области приготовления кофе, поэтому вам не придется тратить время на дорогостоящие эксперименты, мы уже сделали это за вас и предлагаем комплексную услугу.</div>
                    <ul class="whu__list">
                        <li>Бесплатная аренда<br/>при покупке кофе</li>
                        <li>Сервисное<br/>обслуживание</li>
                        <li>Лучший кофе из<br/>Италии</li>
                        <li>Кофе свежей<br/>обжарки</li>
                        <li>Сопутствующие<br/>товары</li>
                    </ul>
                    <div class="whu__bottom"></div>
                </div>
            </section>
            
            <section class="machine" id="b04">
                <div class="container">
                    <h2>КОФЕМАШИНЫ</h2>
                    
                    <div class="machine__item">
                        <div class="machine__slogan yellow"><span>Идеальный вариант для офиса</span></div>
                        <div class="machine__mobile">
                            <div class="machine__type">Кофемашина</div>
                            <div class="machine__name">Kaffit Nizza</div>
                        </div>
                        <div class="machine__image">
                            <div class="machine__image_wrap">
                                <img src="img/machine_01.jpg" class="img-fluid"alt="">
                            </div>
                        </div>
                        
                        <div class="machine__info">
                            <div class="machine__desktop">
                                <div class="machine__type">Кофемашина</div>
                                <div class="machine__name">Kaffit Nizza</div>
                            </div>
                            <div class="machine__description">
                                <div class="machine__description_text">Полностью автоматическая кофемашина, которая дает возможность приготовить широкое разнообразие кофейных напитков с молоком. Каппуччино, латте, латте маккиато, эспрессо, кофе — все эти напитки легко готовятся нажатием всего лишь одной кнопки.</div>
                                <span class="machine__description_link">Все характеристики</span>
                            </div>
                            <ul class="machine__data">
                                <li>
                                    <span>Минимальный срок аренды</span>
                                    <strong>30 дней</strong>
                                </li>
                                <li>
                                    <span>Минимальная закупка кофе</span>
                                    <strong>2 кг/мес</strong>
                                </li>
                            </ul>
                            <div class="machine__price">
                                <span>5 990 ₽/мес</span>
                            </div>
                            <div class="machine__button">
                                <a class="btn btn_black btn_modal" href="#order">Заказать</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="machine__divider"></div>
    
                    <div class="machine__item">
                        <div class="machine__slogan red"><span>отличное решение для horeca</span></div>
                        <div class="machine__mobile">
                            <div class="machine__type">Профессиональная кофемашина + кофемолка</div>
                            <div class="machine__name">Nemox Pro</div>
                        </div>
                        <div class="machine__image">
                            <div class="machine__image_wrap">
                                <img src="img/machine_02.jpg" class="img-fluid"alt="">
                            </div>
                        </div>
                        <div class="machine__info">
                            <div class="machine__desktop">
                                <div class="machine__type">Профессиональная кофемашина + кофемолка</div>
                                <div class="machine__name">Nemox Pro</div>
                            </div>
                            <div class="machine__description">
                                <div class="machine__description_text">Кофемашина  Nemox предназначается для профессионального приготовления кофе. Такие аппараты часто используются в тех заведениях общественного питания, которые желают побаловать гостей тонким ароматом и изысканным вкусом свежего кофе.</div>
                                <span class="machine__description_link">Все характеристики</span>
                            </div>
                            <ul class="machine__data">
                                <li>
                                    <span>Минимальный срок аренды</span>
                                    <strong>60 дней</strong>
                                </li>
                                <li>
                                    <span>Минимальная закупка кофе</span>
                                    <strong>4 кг/мес</strong>
                                </li>
                            </ul>
                            <div class="machine__price">
                                <span>9 990 ₽/мес</span>
                            </div>
                            <div class="machine__button">
                                <a class="btn btn_black btn_modal" href="#order">Заказать</a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </section>
            
            <section class="coffee" id="b05">
                <div class="container">
                    <h2>КОФЕ В ЗЕРНАХ</h2>
    
                    <div class="coffee__box swiper-container">
                        <div class="swiper-wrapper">
    
                            <div class="swiper-slide">
                                <div class="coffee__elem">
                                    <div class="coffee__image">
                                        <img src="img/coffee__01.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="coffee__text">Итальянский кофе Gimoka выпускается с 1982 года, предприятием в городке Андало. Основными преимуществами бренда Джимока являются обжарка кофейных зерен и изготовление ароматного кофе в зернах.  Сравнительно молодая компания сегодня считается одной из самых опытных производителей на кофейном рынке, успешным и быстро развивающимся производителем кофе в зернах.</div>
                                </div>
                            </div>
    
                            <div class="swiper-slide">
                                <div class="coffee__elem">
                                    <div class="coffee__image">
                                        <img src="img/coffee__02.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="coffee__text">Один из лучших брендов, который выпускается с 1920 итальянской компанией Gruppo Garibaldi, основанной семьей Гарибальди.  Сначала компания занималась исключительно обжаркой кофе для других известных итальянских брендов. С 1960 года была создана собственная торговая марка «Garibaldi» и полный цикл производства элитного кофе, начиная от выбора кофейных зерен, заканчивая упаковкой и созданием сопутствующих аксессуаров.</div>
                                </div>
                            </div>
    
                            <div class="swiper-slide">
                                <div class="coffee__elem">
                                    <div class="coffee__image">
                                        <img src="img/coffee__04.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="coffee__text">Lavazza — один из самых популярных брендов кофе в России и одна из крупнейших кофейных компаний в мире. Lavazza имеет четыре производственных предприятия в Италии и семь филиалов в Европе и Америке (Франция, Германия, Испания, Великобритания, Португалия, Австрия, США). Продукты Lavazza продаются в 80 странах мира. В 2010 году товарооборот компании оценивался в 1,1 млрд евро.</div>
                                </div>
                            </div>
    
                            <div class="swiper-slide">
                                <div class="coffee__elem">
                                    <div class="coffee__image">
                                        <img src="img/coffee__03.png" class="img-fluid" alt="">
                                    </div>
                                    <div class="coffee__text">Свежеобжаренный кофе — это идеальный вариант для получения наиболее ароматного напитка.  Молодые и таллантливые ребята из Ju Night Clan никогда не устают эксперементировать и своим упорством добиваются превосходных результатов в обжарке кофе. Выбирая Ju Night Clan вы даже сможете, при желании, разработать свой собственный уникальный бленд кофе.</div>
                                </div>
                            </div>
                            
                        </div>
                        <!-- Add Scrollbar -->
                        <div class="swiper-scrollbar"></div>
                    </div>
                    
                </div>
            </section>
            
            <footer class="footer" id="b06">
                <div class="container">
                    <ul class="footer__row">
                       
                        <li>
                            <a class="footer__logo" href="/">
                                <img src="img/logo.svg" class="img-fluid" alt="">
                            </a>
                            <div class="footer__copy">© 2018 Brewex.<br/> Все права защищены</div>
                        </li>
                        
                        <li>
                            <ul class="footer__contact">
                                <li>
                                    <span>МОСКВА</span>
	                                Огородный пр-д, д.20 с.1
                                </li>
                                <li>
                                    <a href="tel:+74997057378">+7 499 705 73 78</a>
                                    <br/>
                                    <a href="mailto:info@brewex.ru">info@brewex.ru</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li>
                            <div class="footer__social">
                                <span><i class="fa fa-vk"></i></span>
                                <span><i class="fa fa-instagram"></i></span>
                                <span><i class="fa fa-facebook"></i></span>
                                <span><i class="fa fa-youtube"></i></span>
                            </div>
                        </li>
                        
                    </ul>
                    <div class="footer__copy">© 2018 Brewex. Все права защищены</div>
                </div>
            </footer>
        </div>
        
        <div class="hide">
            <div id="order" class="modal">
                <div class="modal__title">Оставьте заявку</div>
	            <div class="modal__subtitle">Наш эксперт с вами свяжется. Или позвоните нам сами: +7 499 705 73 78</div>
    
                <form class="form" name="form" method="POST" action="javascript:void(0);" onsubmit="yaCounterXXXXXX.reachGoal('order'); return true;">
                    <div class="form_group valid name required">
                        <input type="text" class="form_control" name="name" placeholder="Ваше имя">
                    </div>
	                <div class="form_group valid phone required">
		                <input type="text" class="form_control" name="phone" placeholder="Ваш телефон">
	                </div>
                    <div class="form_group">
                        <input type="text" class="form_control" name="email" placeholder="Ваш Email">
                    </div>
        
                    <div class="text-center">
                        <button type="submit" class="btn btn_black btn_send">Отправить</button>
                    </div>
    
                </form>
            </div>
        </div>


        <div class="hide">
            <a href="#thanks" class="btn_thanks btn_modal"></a>
            <div class="modal modal_thanks" id="thanks">
                <div class="modal_thanks__title">Ваша заявка<br/>отправлена.</div>
                <div class="modal_thanks__text">Наш эксперт скоро свяжется с вами.</div>
            </div>

        </div>

        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="js/vendor/swiper/js/swiper.min.js"></script>
        <script src="js/vendor/jquery.scrollTo.min.js"></script>
        <script src="js/vendor/validator.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter50658604 = new Ya.Metrika2({
                            id:50658604,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/tag.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks2");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/50658604" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->


        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127151833-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-127151833-1');
        </script>
    

    </body>
</html>
